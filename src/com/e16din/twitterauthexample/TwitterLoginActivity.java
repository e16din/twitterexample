package com.e16din.twitterauthexample;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TwitterLoginActivity extends Activity {

	private static final String VERIFY_CREDENTIALS = "https://api.twitter.com/1.1/account/verify_credentials.json";

	public final static String CONSUMER_KEY = "8uSu9LauUGJpP3ZJjYc3FA";
	public final static String CONSUMER_SECRET = "UT8bqmhVdJoIwkfmLuNsfKZ6Br1OdHuIJeuIRrihOk";

	public final static String CALLBACK_URL = "http://behappy.fifteen.com/";
	private static final String CALLBACK_DENIED = CALLBACK_URL + "?denied";

	private static final String KEY_OAUTH_VERIFIER = "oauth_verifier";

	private OAuthService oauthService = null;
	private Token requestToken = null;

	private WebView webview = null;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_login);

		webview = (WebView) findViewById(R.id.wv_twitter_login);

		webview.getSettings().setJavaScriptEnabled(true);
		webview.setWebViewClient(new TwitterWebViewClient());

		oauthService = new ServiceBuilder().provider(TwitterApi.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET)
				.callback(CALLBACK_URL).build();

		new WebLoader().execute();

	}

	private class WebLoader extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			requestToken = oauthService.getRequestToken();
			return oauthService.getAuthorizationUrl(requestToken);
		}

		@Override
		protected void onPostExecute(String authUrl) {
			webview.loadUrl(authUrl);
		}
	}

	private class TwitterWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView webview, String url, Bitmap favicon) {

			if (url.startsWith(CALLBACK_DENIED)) {
				setResult(RESULT_CANCELED);
				finish();
				return;
			}

			if (!url.startsWith(CALLBACK_URL)) {
				super.onPageStarted(webview, url, favicon);
				return;
			}
			webview.setVisibility(View.INVISIBLE);
			new RequestSender().execute(url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {

		}
	};

	private class RequestSender extends AsyncTask<String, Void, Token> {
		@Override
		protected Token doInBackground(String... params) {
			Uri uri = Uri.parse(params[0]);
			String verifier = uri.getQueryParameter(KEY_OAUTH_VERIFIER);

			Verifier v = new Verifier(verifier);

			Token accessToken = oauthService.getAccessToken(requestToken, v);
			OAuthRequest req = new OAuthRequest(Verb.GET, VERIFY_CREDENTIALS);
			oauthService.signRequest(accessToken, req);

			return accessToken;
		}

		@Override
		protected void onPostExecute(Token accessToken) {

			SharedPreferences pref = getSharedPreferences("Twitter", Context.MODE_PRIVATE);
			Editor editor = pref.edit();

			editor.putString("accessToken", accessToken.getToken());
			editor.putString("secret", accessToken.getSecret());

			editor.commit();

			setResult(RESULT_OK);
			finish();
		}
	}
}
