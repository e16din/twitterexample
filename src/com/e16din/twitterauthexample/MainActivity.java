package com.e16din.twitterauthexample;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends ActionBarActivity {

	private OnSocialLoginListener twLoginListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			return rootView;
		}
	}

	/**
	 * Twitter
	 */

	public void onBtnLoginClick(View v) {
		twLogin(new OnSocialLoginListener() {
			@Override
			public void onLoginEnd(boolean success, String data) {
				if (success) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							postToTwitter("Yahooooo!");
						}
					}).start();
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0) {
			twLoginListener.onLoginEnd(resultCode == RESULT_OK, null);
		}
	}

	private class TweetSender extends AsyncTask<String, Void, Integer> {
		private Twitter twitter = null;

		public TweetSender(Twitter twitter) {
			this.twitter = twitter;
		}

		@Override
		protected Integer doInBackground(String... params) {
			try {
				twitter.updateStatus(params[0]);
				return 0;
			} catch (TwitterException e) {
				e.printStackTrace();
				return 1;
			}
		}

		@Override
		protected void onPostExecute(Integer stringId) {
			Log.i("info", "stringId: " + stringId);

			if (stringId.intValue() == 0)
				new AlertDialog.Builder(MainActivity.this).setTitle(getString(R.string.app_name))
						.setMessage("Сообщение затвичено!")
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						}).show();
		}
	}

	protected void twLogin(OnSocialLoginListener listener) {
		this.twLoginListener = listener;

		SharedPreferences pref = getSharedPreferences("Twitter", Context.MODE_PRIVATE);

		String token = pref.getString("accessToken", "");
		String secret = pref.getString("secret", "");

		if (TextUtils.isEmpty(token) || TextUtils.isEmpty(secret)) {
			openTwitterSession();
			return;
		}

		twLoginListener.onLoginEnd(true, null);
	}

	public void openTwitterSession() {
		Intent intent = new Intent();
		intent.setClass(this, TwitterLoginActivity.class);
		startActivityForResult(intent, 0);
	}

	protected void postToTwitter(String message) {

		SharedPreferences pref = getSharedPreferences("Twitter", Context.MODE_PRIVATE);

		String token = pref.getString("accessToken", "");
		String secret = pref.getString("secret", "");

		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(TwitterLoginActivity.CONSUMER_KEY);
		builder.setOAuthConsumerSecret(TwitterLoginActivity.CONSUMER_SECRET);
		builder.setOAuthAccessToken(token);
		builder.setOAuthAccessTokenSecret(secret);
		Configuration conf = builder.build();

		Twitter t = new TwitterFactory(conf).getInstance();

		new TweetSender(t).execute(message);
	}

	public interface OnSocialLoginListener {
		public void onLoginEnd(boolean success, String data);
	}

}
